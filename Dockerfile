ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/opensource/python/python38
ARG BASE_TAG=3.8

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

USER root

RUN dnf update -y && \
    dnf install -y --nodocs git && \
    dnf -y clean all && \
    rm -rf /var/cache/dnf

COPY *.tar.gz /
COPY *.whl /wheel/

RUN pip install --no-index --upgrade --find-links=/wheel/ cffi cryptography Jinja2 MarkupSafe packaging pycparser \
       pyparsing PyYAML resolvelib && \
    pip install /ansible-core.tar.gz ansible.tar.gz

RUN rm -rf *.tar.gz /wheel && \
    find /usr/local/lib/python3.8/site-packages/ansible_collections/ -name "*.pem" | xargs rm -f && \
    find /usr/local/lib/python3.8/site-packages/ansible_collections/ -name "*.key" | xargs rm -f && \
    rm -f /usr/local/lib/python3.8/site-packages/ansible_collections/cisco/ios/tests/integration/targets/ios_user/files/test_rsa && \
    rm -f /usr/local/lib/python3.8/site-packages/ansible_collections/cisco/iosxr/tests/integration/targets/iosxr_user/files/private && \
    rm -f /usr/local/lib/python3.8/site-packages/ansible_collections/infinidat/infinibox/Makefile && \
    chmod -s /usr/libexec/openssh/ssh-keysign

USER 1001
WORKDIR /ansible

HEALTHCHECK NONE

CMD [ "ansible-playbook", "--version" ]
